﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace test
{
    public class Authentication
    {
        private struct Person
        {
            public string Login;
            public string Password;
            public string Role;
        }

        private static List<Person> people = new List<Person>
        {
            new Person {Login = "Taria", Password = "228", Role = "admin"},
            new Person {Login = "Mykyta", Password = "1337", Role = "master"},
            new Person {Login = "pepegAndrey", Password = "2mbh3RvnbBRXku", Role = "slave"}
        };

        public static int Login(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                throw new Exception("Incorrect input field");
            if (!GetIdentity(username, password))
                throw new Exception("User not found");

            return 0;
        }

        private static bool GetIdentity(string username, string password)
        {
            Person person = people.FirstOrDefault(x => x.Login == username && x.Password == password);
            return person.Login != null;
        }
    }
}