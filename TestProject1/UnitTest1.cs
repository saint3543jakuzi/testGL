using NUnit.Framework;
using test;

namespace TestProject1
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestItsOK()
        {
            Assert.AreEqual(0, Authentication.Login("Mykyta", "1337"));
        }
        
        [Test]
        public void TestItsNotOKUsername()
        {
            Assert.AreEqual(0, Authentication.Login("Mykytaaaaaaaaaaaaaaaaaa", "1337"));
        }
        
        [Test]
        public void TestItsNotOKShoTyVoobshcheHochesh()
        {
            Assert.AreEqual(0, Authentication.Login("Mykyta", ""));
        }
    }
}